package foxminded.f1.dao;

import foxminded.f1.buisness.PilotCard;
import org.junit.jupiter.api.Test;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DaoTest {
    Map<String, PilotCard> referenceQualificationMap = new LinkedHashMap<>();
    Map<String, PilotCard> readedFromFilesQualificationMap = new LinkedHashMap<>();
    Dao dao = new Dao();

    public DaoTest() throws URISyntaxException {
    }

    @Test
    void daoTest_checks_checksReadingFromFiles() {
        referenceQualificationMap.clear();
        referenceQualificationMap.put("DRR", new PilotCard("DRR", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "14:12.054", "15:24.067", 0L));
        referenceQualificationMap.put("SVF", new PilotCard("SVF", "Sebastian Vettel", "FERRARI", "02:58.917", "04:11.332", 0L));
        referenceQualificationMap.put("LHM", new PilotCard("LHM", "Lewis Hamilton", "MERCEDES", "18:20.125", "19:32.585", 0L));

        readedFromFilesQualificationMap = dao.readAllFilesAndReturnNewFilledResultsMap();
        assertEquals(referenceQualificationMap, readedFromFilesQualificationMap);
    }
}