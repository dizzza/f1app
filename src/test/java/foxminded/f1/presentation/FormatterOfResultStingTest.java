package foxminded.f1.presentation;

import foxminded.f1.buisness.PilotCard;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FormatterOfResultStingTest {
    List<PilotCard> handFilledQualificationList = new ArrayList<>();
    private final FormatterOfResultString formatterOfResultString = new FormatterOfResultString();

    @Test
    void formatterTest_comparesGeneratedStringToReferenceString() {
        handFilledQualificationList.clear();
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));
        handFilledQualificationList.add(new PilotCard("", "Daniel Ricciardo", "RED BULL RACING TAG HEUER", "0", "0", 72013L));
        handFilledQualificationList.add(new PilotCard("", "Sebastian Vettel", "FERRARI", "0", "0", 72415L));

        String expectedResult = "1.   Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "2.   Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "3.   Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "4.   Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "5.   Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "6.   Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "7.   Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "8.   Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "9.   Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "10.  Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "11.  Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "12.  Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "13.  Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "14.  Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "15.  Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "--------------------------------------------------------------\n" +
            "16.  Sebastian Vettel  | FERRARI                   | 01:12.415\n" +
            "17.  Daniel Ricciardo  | RED BULL RACING TAG HEUER | 01:12.013\n" +
            "18.  Sebastian Vettel  | FERRARI                   | 01:12.415\n";

        String formattedResult = formatterOfResultString.takeResultFormattedString(handFilledQualificationList);
        assertEquals(expectedResult, formattedResult);
    }
}