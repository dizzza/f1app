package foxminded.f1.buisness;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QualificationPositionSorterTest {
    List<PilotCard> referenceSortedQualificationList = new ArrayList<>();
    Map<String, PilotCard> qualificationDataMap = new LinkedHashMap<>();
    QualificationPositionSorter qualificationPositionSorter = new QualificationPositionSorter();

    @Test
    void sorterTest_comparesSortedListFromMapToReferenceSortedList() {
        referenceSortedQualificationList.clear();
        referenceSortedQualificationList.add(new PilotCard("APF", "A_First Pilot", "First Team", "0", "0", 720L));
        referenceSortedQualificationList.add(new PilotCard("BPS", "B_Second Pilot", "Second Team", "0", "0", 7200L));
        referenceSortedQualificationList.add(new PilotCard("DPF", "C_Third Pilot", "Third Team", "0", "0", 72000L));
        referenceSortedQualificationList.add(new PilotCard("CPF", "D_Fourth Pilot", "Fourth Team", "0", "0", 720000L));

        qualificationDataMap.clear();
        qualificationDataMap.put("BPS", new PilotCard("BPS", "B_Second Pilot", "Second Team", "0", "0", 7200L));
        qualificationDataMap.put("APF", new PilotCard("APF", "A_First Pilot", "First Team", "0", "0", 720L));
        qualificationDataMap.put("CPF", new PilotCard("CPF", "D_Fourth Pilot", "Fourth Team", "0", "0", 720000L));
        qualificationDataMap.put("DPF", new PilotCard("DPF", "C_Third Pilot", "Third Team", "0", "0", 72000L));

        List<PilotCard> sortedQualificationList = qualificationPositionSorter.sortQualificationDataAndReturnSortedDataList(qualificationDataMap);
        assertEquals(referenceSortedQualificationList, sortedQualificationList);
    }
}