package foxminded.f1.buisness;

import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LapTimeCalculatorTest {
    LapTimeCalculator lapTimeCalculator = new LapTimeCalculator();
    Map<String, PilotCard> referenceSortedQualificationMap = new LinkedHashMap<>();
    Map<String, PilotCard> qualificationMapToSort = new LinkedHashMap<>();
    Map<String, PilotCard> sortedQualificationMap = new LinkedHashMap<>();

    @Test
    void lapTimeCalculatorTest_checksThatCalculatorModuleRecordsCorrectLapTimeToMap() {
        referenceSortedQualificationMap.clear();
        referenceSortedQualificationMap.put("APF", new PilotCard("APF", "A_First Pilot", "First Team", "03:15.000", "03:15.145", 145L));
        referenceSortedQualificationMap.put("BPS", new PilotCard("BPS", "B_Second Pilot", "Second Team", "16:10.000", "16:11.648", 1648L));
        referenceSortedQualificationMap.put("DPF", new PilotCard("DPF", "C_Third Pilot", "Third Team", "18:00.150", "18:37.785", 37635L));
        referenceSortedQualificationMap.put("CPF", new PilotCard("CPF", "D_Fourth Pilot", "Fourth Team", "15:01.150", "18:00.000", 178850L));

        qualificationMapToSort.clear();
        qualificationMapToSort.put("APF", new PilotCard("APF", "A_First Pilot", "First Team", "03:15.000", "03:15.145", 0L));
        qualificationMapToSort.put("BPS", new PilotCard("BPS", "B_Second Pilot", "Second Team", "16:10.000", "16:11.648", 0L));
        qualificationMapToSort.put("DPF", new PilotCard("DPF", "C_Third Pilot", "Third Team", "18:00.150", "18:37.785", 0L));
        qualificationMapToSort.put("CPF", new PilotCard("CPF", "D_Fourth Pilot", "Fourth Team", "15:01.150", "18:00.000", 0L));

        sortedQualificationMap = lapTimeCalculator.calculateLapTimes(qualificationMapToSort);
        assertEquals(referenceSortedQualificationMap, sortedQualificationMap);
    }
}
