package foxminded.f1.dao;

import foxminded.f1.buisness.PilotCard;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class Dao {
    private final Path abbreviationFilePath = Paths.get(Objects.requireNonNull(this.getClass().getClassLoader().getResource("abbreviations.txt")).toURI());
    private final Path startFilePath = Paths.get(Objects.requireNonNull(this.getClass().getClassLoader().getResource("start.log")).toURI());
    private final Path endFilePath = Paths.get(Objects.requireNonNull(this.getClass().getClassLoader().getResource("end.log")).toURI());
    private final String abbrSeparatorString = "_";
    private final String startSeparatorString = "2018-05-24_12:";
    private final String endSeparatorString = "2018-05-24_12:";

    public Dao() throws URISyntaxException {
    }

    public Map<String, PilotCard> readAllFilesAndReturnNewFilledResultsMap() {
        return addEndFileDataToResultsMap(addStartFileDataToResultsMap(addAbbrFileDataToResultsMap()));
    }

    private Map<String, PilotCard> addAbbrFileDataToResultsMap() {
        Map<String, PilotCard> allPilotsQualificationDTO = new LinkedHashMap<>();
        try (Stream<String> myStream = Files.lines(abbreviationFilePath)) {
            myStream.map(line -> line.split(abbrSeparatorString))
                .forEach(word -> allPilotsQualificationDTO.put(word[0], new PilotCard(word[0], word[1], word[2], "", "", 0L)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allPilotsQualificationDTO;
    }

    private Map<String, PilotCard> addStartFileDataToResultsMap(Map<String, PilotCard> qualificationDTO) {
        try (Stream<String> myStream = Files.lines(startFilePath)) {
            myStream.map(line -> line.split(startSeparatorString))
                .forEach(word -> (qualificationDTO.get(word[0])).setStartTime(word[1]));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return qualificationDTO;
    }

    private Map<String, PilotCard> addEndFileDataToResultsMap(Map<String, PilotCard> qualificationDTO) {
        try (Stream<String> myStream = Files.lines(endFilePath)) {
            myStream.map(line -> line.split(endSeparatorString))
                .forEach(word -> (qualificationDTO.get(word[0])).setFinishTime(word[1]));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return qualificationDTO;
    }
}