package foxminded.f1.presentation;

import foxminded.f1.buisness.PilotCard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class FormatterOfResultString {
    int positionOfDemarcationLine = 15;
    String demarcationLineValue = "--------------------------------------------------------------";

    public String takeResultFormattedString(List<PilotCard> sortedQualificationList) {
        SimpleDateFormat resultInMSecDateFormat = new SimpleDateFormat("S");
        SimpleDateFormat startAndFinishTimeFormat = new SimpleDateFormat("mm:ss.SSS");
        StringBuilder sb = new StringBuilder();
        AtomicInteger counter = new AtomicInteger(1);

        sortedQualificationList.forEach(s -> {
            try {
                sb.append(String.format("%-4s %-17s | %-25s | %s\n", (counter.getAndIncrement() + "."), s.getPilotName(), s.getTeam(),
                    startAndFinishTimeFormat.format(resultInMSecDateFormat.parse(s.getLapTimeInMSec().toString()))));
                if (counter.get() == positionOfDemarcationLine + 1) {
                    sb.append(demarcationLineValue).append("\n");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        return sb.toString();
    }
}