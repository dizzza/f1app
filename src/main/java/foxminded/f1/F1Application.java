package foxminded.f1;

import foxminded.f1.buisness.LapTimeCalculator;
import foxminded.f1.buisness.PilotCard;
import foxminded.f1.buisness.QualificationPositionSorter;
import foxminded.f1.dao.Dao;
import foxminded.f1.presentation.FormatterOfResultString;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

public class F1Application {
    public static void main(String[] args) throws URISyntaxException {
        Map<String, PilotCard> qualificationResults;
        List<PilotCard> sortedQualificationResultsList;

        Dao dao = new Dao();
        LapTimeCalculator lapTimeCalculator = new LapTimeCalculator();
        QualificationPositionSorter positionSorter = new QualificationPositionSorter();
        FormatterOfResultString formatterOfResultString = new FormatterOfResultString();

        qualificationResults = lapTimeCalculator.calculateLapTimes(dao.readAllFilesAndReturnNewFilledResultsMap());
        sortedQualificationResultsList = positionSorter.sortQualificationDataAndReturnSortedDataList(qualificationResults);
        System.out.println(formatterOfResultString.takeResultFormattedString(sortedQualificationResultsList));
    }
}