package foxminded.f1.buisness;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QualificationPositionSorter {
    public List<PilotCard> sortQualificationDataAndReturnSortedDataList(Map<String, PilotCard> qualificationDataMap) {
        return qualificationDataMap.values().stream()
            .sorted(Comparator.comparingLong(PilotCard::getLapTimeInMSec))
            .collect(Collectors.toList());
    }
}