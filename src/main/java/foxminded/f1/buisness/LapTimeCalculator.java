package foxminded.f1.buisness;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class LapTimeCalculator {
    SimpleDateFormat startAndFinishTimeFormat = new SimpleDateFormat("mm:ss.SSS");

    public Map<String, PilotCard> calculateLapTimes(Map<String, PilotCard> qualificationDataMap) {
        qualificationDataMap.values()
            .forEach(s -> {
                try {
                    s.setLapTimeInMSec(startAndFinishTimeFormat.parse(s.getFinishTime()).getTime() - startAndFinishTimeFormat.parse(s.getStartTime()).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });
        return qualificationDataMap;
    }
}