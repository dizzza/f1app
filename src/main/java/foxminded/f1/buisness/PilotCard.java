package foxminded.f1.buisness;

import java.util.Objects;

public class PilotCard {
    private final String abbreviationOfNameAndTeam;
    private final String pilotName;
    private final String team;
    private String startTime;
    private String finishTime;
    private Long lapTimeInMSec;

    public PilotCard(String abbreviationOfNameAndTeam, String pilotName, String team, String startTime, String finishTime, Long lapTimeMSec) {
        this.abbreviationOfNameAndTeam = abbreviationOfNameAndTeam;
        this.pilotName = pilotName;
        this.team = team;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.lapTimeInMSec = lapTimeMSec;
    }

    public String getPilotName() {
        return pilotName;
    }

    public String getTeam() {
        return team;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public Long getLapTimeInMSec() {
        return lapTimeInMSec;
    }

    public void setLapTimeInMSec(Long lapTimeInMSec) {
        this.lapTimeInMSec = lapTimeInMSec;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PilotCard)) return false;
        PilotCard pilotCard = (PilotCard) o;
        return Objects.equals(abbreviationOfNameAndTeam, pilotCard.abbreviationOfNameAndTeam) && Objects.equals(getPilotName(),
            pilotCard.getPilotName()) && Objects.equals(getTeam(), pilotCard.getTeam()) && Objects.equals(getStartTime(),
            pilotCard.getStartTime()) && Objects.equals(getFinishTime(), pilotCard.getFinishTime()) && Objects.equals(getLapTimeInMSec(),
            pilotCard.getLapTimeInMSec());
    }

    @Override
    public int hashCode() {
        return Objects.hash(abbreviationOfNameAndTeam, getPilotName(), getTeam(), getStartTime(), getFinishTime(), getLapTimeInMSec());
    }
}